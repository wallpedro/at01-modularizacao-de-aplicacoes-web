-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Mar-2019 às 22:57
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2`

CREATE DATABASE IF NOT EXISTS lp2 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE lp2;
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL,
  `img` varchar(50) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `preco` varchar(8) NOT NULL,
  `desc` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `img`, `nome`, `preco`, `desc`) VALUES
(2, 'assets/img/cardapio/img_1.jpg', 'Burger com cebola', '19,90', 'Delicioso hamburguer com cebola'),
(3, 'assets/img/cardapio/img_2.jpg', 'Burger com salada', '25,00', 'Delicioso hambúrguer com alface'),
(4, 'assets/img/cardapio/img_3.jpg', 'Burger com alface e tomate', '32,00', 'Delicioso hambúrguer com alface e tomate.'),
(6, 'assets/img/cardapio/img_4.jpg', 'Burger de Frango', '32,00', 'Delicioso hambúrguer de frango.'),
(11, 'assets/img/cardapio/img_5.jpg', 'Burger vegetariano', '999,00', 'Delicioso hambúrguer vegetariano. ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
