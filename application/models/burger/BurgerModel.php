<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class BurgerModel extends CI_Model{
        
        public function get_data($id, $table){
            $sql = "SELECT * FROM $table WHERE id = $id";
            $res = $this->db->query($sql);
            $data = $res->result_array();
            return $data[0];
        }
        public function novo_produto(){
            if(sizeof($_POST) == 0) return;
            $data = $this->input->post();
            $this->db->insert('produtos',$data);
            redirect(base_url('/Main/Produtos'));
        }
        public function edita_produto($id){
            if(sizeof($_POST) == 0) return;
            $data = $this->input->post();
    
            $where = "id = $id";
            
            $sql = $this->db->update_string('produtos',$data, $where);
            
            $this->db->query($sql);
            redirect(base_url('/Main/Produtos'));
        }
        public function numero_registros($table){
            $sql = "SELECT * FROM $table";
            $res = $this->db->query($sql);
            $data = $res->num_rows();
            return $data;
        }
        public function get_db_data($table){
            $sql = "SELECT * FROM $table";
            $res = $this->db->query($sql);
            return $res;
        }

        public function apaga_produto($id){
            $sql = "DELETE FROM produtos WHERE id= $id";
            $this->db->query($sql);
            redirect(base_url('/Main/Produtos'));
        }

        public function home_static_data($id){
            $data['slide_1'] = "assets/img/home/img_1.jpg";
            $data['slide_2'] = "assets/img/home/img_2.jpg";
            $data['slide_3'] = "assets/img/home/img_3.jpg";
            $data['slide_4'] = "assets/img/home/img_4.jpg";
            $data['image'] = "assets/img/logo.png";
            if($id == 1){
                $data['card_image'] = "assets/img/home_cards/img_contato.jpg";
                $data['card_title'] = "Contato";
                $data['card_text'] = "Fale conosco! Oferecemos suporte via telefone e e-mail.";
                $data['card_button'] = "Fale conosco";
                $data['card_link'] = "Main/Contato";
            }
            if($id == 2){
                $data['card_image'] = "assets/img/home_cards/img_sobre.png";
                $data['card_title'] = "Sobre nós";
                $data['card_text'] = "Saiba mais sobre a história do maior hambúrguer do Brasil";
                $data['card_button'] = "Nossa história";
                $data['card_link'] = "Main/About";
            }
            if($id == 3){
                $data['card_image'] = "assets/img/home_cards/img_produtos.png";
                $data['card_title'] = "Produtos";
                $data['card_text'] = "Conheça todos nossos deliciosos hambúrgueres.";
                $data['card_button'] = "Produtos";
                $data['card_link'] = "Main/Produtos";
            }
            return $data;
        }
        public function about_static_data(){
            $data['image'] = "assets/img/about_bg.jpg";
            $data['title'] = "Sobre nós";
            $data['text'] = "Somos uma hamburgueria que se dedicou a criar o maior e melhor hambúrguer da região";
            $data['link'] = "Main/Produtos";
            $data['button_text'] = "Conheça nossos produtos";

            return $data;
        }

        public function contact_static_data(){
            $data['maps_embed'] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d467692.04866725317!2d-46.875483694121776!3d-23.681531505430495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce448183a461d1%3A0x9ba94b08ff335bae!2zU8OjbyBQYXVsbywgU1A!5e0!3m2!1spt-BR!2sbr!4v1552250268418";
            $data['end'] = "São Paulo - SP";
            $data['pais'] = "Brasil";
            $data['tel'] = "(11) 94294-3063";
            $data['h_atend'] = "Seg à Sex, 8:00 às 18:00";
            $data['info_mail'] = "informações@hamburgao.com";
            $data['sale_mail'] = "vendas@hamburgao.com";
            $data['title'] = "Fale Conosco";
            return $data;
        }
    }