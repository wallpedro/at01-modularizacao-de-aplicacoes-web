<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        
        <div class="carousel-item active">
            <img class="d-block w-100" src="<?= base_url($slide_1)?>" alt="Primeira Imagem">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url($slide_2)?>" alt="Segunda Imagem">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url($slide_3)?>" alt="Terceira Imagem">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url($slide_4)?>" alt="Quarta Imagem">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Próximo</span>
    </a>
</div>