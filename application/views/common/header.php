<!DOCTYPE html>
<html lang="pt">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>HAMBURGÃO</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <link rel="icon" href="<?= base_url('assets/img/icon.png') ?>">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
</head>

<body class="orange lighten-5">