<footer class="page-footer font-small light-green">
    <div class="container">
        <div class="row">
            <div class="col-md-12 py-5">
                <div class="mb-5 flex-center">
                    <a href="https://www.facebook.com" class="fb-ic">
                        <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a href="https://twitter.com" class="tw-ic">
                        <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="#">Pedro Muro</a>
    </div>
  </footer>
