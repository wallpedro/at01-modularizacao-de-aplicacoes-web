<nav class="mb-1 navbar navbar-expand-lg navbar-dark light-green">
    <a class="navbar-brand bg-orange" href="<?= base_url('Main')?>" style="color: green">Hamburgão</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?= base_url('Main/Produtos')?>">Produtos
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Main/About')?>">Sobre nós</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('Main/Contato')?>">Contato</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Administrativo</a>
                <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
                    <a class="dropdown-item" href="<?= base_url('Main/novo_produto')?>">Criar Produto</a>
                    <a class="dropdown-item" href="<?= base_url('Main/select_produto/1')?>">Editar Produto</a>
                    <a class="dropdown-item" href="<?= base_url('Main/select_produto/2')?>">Apagar Produto</a>
                </div>
            </li>
        </ul>
    </div>
</nav>