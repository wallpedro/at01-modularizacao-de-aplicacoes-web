<div class="card">
    <div class="view overlay">
        <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 500px">
            <iframe class="col-sm-12 mt-3" src="<?= $maps_embed ?>" frameborder="0"
            style="border:0; height: 500px;" allowfullscreen></iframe>
        </div>
    </div>
    <div class="card-body light-green white-text rounded-bottom">
        <a class="activator waves-effect mr-4"><i class="fas fa-share-alt white-text"></i></a>
        <h4 class="card-title"><? $title ?></h4>
        <hr class="hr-light">
        <p class="card-text white-text mb-4">Endereço: <?= $end ?></p>
        <p class="card-text white-text mb-4">Telefone: <?= $tel ?></p>
        <p class="card-text white-text mb-4">Horário de atendimento: <?= $h_atend ?></p>
        <p class="card-text white-text mb-4">E-mail para informações: <?= $info_mail ?></p>
        <p class="card-text white-text mb-4">E-mail para vendas: <?= $sale_mail ?></p>
    </div>
</div>
