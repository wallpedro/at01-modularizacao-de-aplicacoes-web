<div class="container" style="min-height: 100vh;">
            <?php
                $i = 0;
                if($tipo == 1){
                    $url = "edita_produto";
                }
                elseif($tipo == 2){
                    $url = "exclui_produto";
                }
                foreach($dados->result() as $prod){
                    if(($i == 0) || ($i % 4 == 0)) echo "<div class='row mt-2'>";
                    echo "<div class='col-sm-3 mx-auto'>";
                    if($prod->nome == NULL) $prod->nome = "Sem nome";
                    echo "<a class='btn peach-gradient btn-lg btn-block' href='".base_url('/Main/'.$url.'/'.$prod->id)."'>".$prod->nome."</a>";
                    echo "</div>";
                    $i++;
                    if(($i == 0) || ($i % 4 == 0)) echo "</div>";
                }
                while($i % 4 != 0){
                    echo "<div class='col-sm-3 mx-auto'></div>";
                    $i++;
                }
                echo "</div>";
            ?>
</div>