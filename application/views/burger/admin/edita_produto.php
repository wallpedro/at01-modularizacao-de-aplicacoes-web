<div class="container mt-3" style="min-height: 100vh;">
    <div class="row">
        <div class="col-sm-12">
            <?php
                if($tipo == 1){ //Criação
                    $title = "Novo produto";
                    $but = "Criar";
                }elseif($tipo == 2){ //edição
                    $title = "Editar produto";
                    $but = "Editar";
                }
            ?>
            <form class="text-center border border-light p-5 white" method="post">
                <p class="h4 mb-4"><?= $title?></p>
                Foto: <input max-length="49" required placeholder="Endereço da Foto" type="text" value="<?= isset($img)? $img : '' ?>" id="img" name="img" class="form-control mb-4"> 
                Nome: <input max-length="29" required placeholder="Nome" type="text" value="<?= isset($nome)? $nome : '' ?>" id="nome" name="nome" class="form-control mb-4">
                Preço (R$): <input max-length="7" required placeholder="Preço (R$)" type="text" value="<?= isset($preco)? $preco : '' ?>" id="preco" name="preco" class="form-control mb-4">
                Descrição: <input max-length="299" required placeholder="Descrição" type="text" value="<?= isset($desc)? $desc : '' ?>" id="desc" name="desc" class="form-control mb-4">
                <button class="btn btn-warning btn-block my-4" type="submit"><?= $but?></button>
            </form>
        </div>
    </div>
</div>