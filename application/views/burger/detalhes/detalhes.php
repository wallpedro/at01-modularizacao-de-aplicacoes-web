<div class="card mt-3 mb-3">
    <div class="view overlay">
        <img class="card-img-top" src="<?= base_url($img) ?>" alt="Imagem do produto">
        <a href="#!">
            <div class="mask rgba-white-slight"></div>
        </a>
    </div>
    <div class="card-body">
        <h4 class="card-title"><?= $nome ?></h4>
        <p class="card-text">Preço: R$<?= $preco ?></p>
        <p class="card-text"><?= $desc ?></p>
        <a href="<?= base_url('Main/Produtos')?>" class="btn btn-light-green">Voltar</a>
    </div>
</div>