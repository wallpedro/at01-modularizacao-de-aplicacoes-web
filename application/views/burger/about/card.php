<div class="card">
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url($image) ?>" alt="Card image cap">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
    </div>

    <div class="card-body light-green white-text rounded-bottom">
        <h4 class="card-title"><?= $title ?></h4>
        <hr class="hr-light">
        <p class="card-text white-text mb-4"><?= $text ?></p>
        <a href="<?= base_url($link) ?>" class="white-text d-flex justify-content-end"><h5><?=$button_text ?><i class="fas fa-angle-double-right"></i></h5></a>
    </div>
</div>