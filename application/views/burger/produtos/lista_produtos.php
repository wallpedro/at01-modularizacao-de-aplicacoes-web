<?php
    $i = 0;
    foreach($data->result() as $res){
        if(($i == 0) || ($i % 3 == 0)) echo "<div class='row'>";
        echo "<div class='col-sm-4 mx-auto'>";
        $this->load->view('burger/produtos/card_produto', $res);
        echo "</div>";
        $i++;
        if(($i == 0) || ($i % 3 == 0)) echo "</div>";
    }
    while($i % 3 != 0){
        echo "<div class='col-sm-4 mx-auto'></div>";
        $i++;
    }
    echo "</div>";
?>