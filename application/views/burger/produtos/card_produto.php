<div class="card mt-3 mb-3">
    <div class="view overlay">
        <img class="card-img-top" src="<?= base_url($img) ?>" alt="Imagem do cartão">
        <a href="#!">
            <div class="mask rgba-white-slight"></div>
        </a>
    </div>
    <div class="card-body">
        <h4 class="card-title"><?= $nome ?></h4>
        <p class="card-text">R$ <?= $preco ?></p>
        <a href="<?= base_url('/Main/detalhes/'.$id) ?>" class="btn btn-light-green">Mais detalhes</a>
    </div>
</div>