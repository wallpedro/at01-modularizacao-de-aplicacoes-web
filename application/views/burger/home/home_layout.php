<div class="container">
    <div class="row">
        <div class="col-sm-12 mx-auto mt-3">
            <?= $jumbo ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 mx-auto mt-3 mb-3">
            <?= $carousel ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4 mx-auto">
            <?= $card_1 ?>
        </div>
        <div class="col-sm-12 col-md-4 mx-auto">
            <?= $card_2 ?>
        </div>
        <div class="col-sm-12 col-md-4 mx-auto">
            <?= $card_3 ?>
        </div>
    </div>
</div>