<div class="card mt-3 mb-3">
    <div class="view overlay">
        <img class="card-img-top" src="<?= base_url($card_image) ?>" alt="Imagem do cartão">
        <a href="#!">
            <div class="mask rgba-white-slight"></div>
        </a>
    </div>
    <div class="card-body">
        <h4 class="card-title"><?= $card_title ?></h4>
        <p class="card-text"><?= $card_text ?></p>
        <a href="<?= base_url($card_link) ?>" class="btn btn-light-green"><?= $card_button ?></a>
    </div>
</div>