<div class="jumbotron text-center">
  <div class="view overlay my-4">
    <img src="<?= base_url($image)?>" class="img-fluid" alt="Logo">
    <a href="#">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>
  <a class="fa-lg p-2 m-2 li-ic"><i class="fab fa-linkedin-in grey-text"></i></a>
  <a class="fa-lg p-2 m-2 tw-ic"><i class="fab fa-twitter grey-text"></i></a>
  <a class="fa-lg p-2 m-2 fb-ic"><i class="fab fa-facebook-f grey-text"></i></a>
</div>
