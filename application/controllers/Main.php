<?php
    class Main extends CI_Controller{
        
        public function Index(){
            $id = 1;
            //Views comuns
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            //Carrega os parametros da pagina inicial
            $this->load->model('burger/BurgerModel');
            $dados = $this->BurgerModel->home_static_data($id);
            $vetor['jumbo'] = $this->load->view('burger/home/jumbo', $dados, true);
            $vetor['carousel'] = $this->load->view('common/carousel', $dados, true);
            $dados = $this->BurgerModel->home_static_data($id);
            $vetor['card_1'] = $this->load->view('burger/home/card', $dados, true);
            $id = 2;
            $dados = $this->BurgerModel->home_static_data($id);
            $vetor['card_2'] = $this->load->view('burger/home/card', $dados, true);
            $id = 3;
            $dados = $this->BurgerModel->home_static_data($id);
            $vetor['card_3'] = $this->load->view('burger/home/card', $dados, true);
            $this->load->view('burger/home/home_layout', $vetor);
            $this->load->view('common/footer_view');
            $this->load->view('common/footer');
        }

        public function Produtos(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('burger/BurgerModel');
            
            
            $data['data'] = $this->BurgerModel->get_db_data('produtos');
            
            $vetor['produtos'] = $this->load->view('burger/produtos/lista_produtos', $data, true);

            
            $this->load->view('burger/produtos/produtos_layout', $vetor);

            $this->load->view('common/footer_view');
            $this->load->view('common/footer');
        }

        public function About(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('burger/BurgerModel');

            $data = $this->BurgerModel->about_static_data();

            $vetor['card'] = $this->load->view('burger/about/card',$data,true);
            $this->load->view('burger/about/about_layout', $vetor);

            $this->load->view('common/footer_view');
            $this->load->view('common/footer');
        }

        public function Contato(){
            $this->load->view('common/header');
            $this->load->view('common/navbar');
            
            $this->load->model('burger/BurgerModel');

            $data = $this->BurgerModel->contact_static_data();
            $vetor['contact'] = $this->load->view('burger/contact/contact_section', $data, true);
            $this->load->view('burger/contact/contact_layout', $vetor);
            $this->load->view('common/footer_view');
            $this->load->view('common/footer');
        }

        public function detalhes($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('burger/BurgerModel');
            $data = $this->BurgerModel->get_data($id, 'produtos');
            $vetor['card'] = $this->load->view('burger/detalhes/detalhes', $data, true);
            $this->load->view('burger/detalhes/detalhes_layout', $vetor);

            $this->load->view('common/footer_view');
            $this->load->view('common/footer');


        }


        public function edita_produto($id){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('burger/BurgerModel');
            $data = $this->BurgerModel->get_data($id, 'produtos');
            $data['tipo'] = 2; //edição
            $this->load->view('burger/admin/edita_produto', $data);
            $this->BurgerModel->edita_produto($id);
            $this->load->view('common/footer_view');
            $this->load->view('common/footer');
        }

        public function novo_produto(){
            $data['tipo'] = 1;
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            
            $this->load->view('burger/admin/edita_produto', $data);
            
            $this->load->model('burger/BurgerModel');
            $this->BurgerModel->novo_produto();

            $this->load->view('common/footer_view');
            $this->load->view('common/footer');
        }

        public function select_produto($tipo){
            $this->load->view('common/header');
            $this->load->view('common/navbar');

            $this->load->model('burger/BurgerModel');
            $data['dados'] = $this->BurgerModel->get_db_data('produtos');
            $data['tipo'] = $tipo;
            $this->load->view('burger/admin/seleciona_produto', $data);
            $this->load->view('common/footer_view');
            $this->load->view('common/footer');
        }
        
        public function exclui_produto($id){
            $this->load->model('burger/BurgerModel');
            $this->BurgerModel->apaga_produto($id);
        }
        
    }
?>